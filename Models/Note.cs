﻿namespace FMF.Toetscie.Models;

public class Note : IFile
{
    public string? Route { get; set; }
    public DateTime? Date { get; set; }
    public string? Type { get; set; }
    public string? Author { get; set; }
}