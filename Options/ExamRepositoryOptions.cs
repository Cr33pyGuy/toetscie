﻿namespace FMF.Toetscie.Options;

public class ExamRepositoryOptions
{
    public const string ExamRepo = "ExamRepo";

    public string ExamsFolder { get; set; } = string.Empty;
}