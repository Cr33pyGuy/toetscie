using FMF.Toetscie.Models;

namespace FMF.Toetscie.Repositories;

public interface IExamRepository
{
    /*
     * This interface is an abstraction of objects in some storage system. The controller operates based on this interface
     * alone without knowing how this is actually implemented. We can supply a different implementation of this interface
     * to the controller and any other class that needs it injected in ../Program.cs.
     *
     * To not have to browse through specific implementations of this interface when using its methods, they need a docstring.
     * Especially for the exceptions because they sometimes need to be caught. The exact return and parameter types however
     * do not need to be in the docstring as they are part of the function declaration.
     */
    
    
    /// <summary>
    /// Asynchronously returns a List of all the degrees found in the directory specified in appsettings.<seealso cref="Degree"/>
    /// <remarks>To save on loading times, these degrees do not have any courses.</remarks>
    /// </summary>
    /// <returns>A <see cref="Task{List}"/> that upon completion returns a <see cref="List{Degree}"/>.</returns>
    public Task<List<Degree>> ListDegreesAsync();
    
    
    /// <summary>
    /// Asynchronously returns the degree corresponding to given degree name.
    /// <remarks>To save on loading times, the courses of this degree do not have any exams.</remarks>
    /// </summary>
    /// <param name="degreeName">Name of the degree.</param>
    /// <exception cref="T:System.ArgumentException">
    /// <paramref name="degreeName" /> forms an invalid path.</exception>
    /// <exception cref="T:System.Collections.Generic.KeyNotFoundException">No corresponding degree was found.</exception>
    /// <returns>A <see cref="Task{Degree}"/> that upon completion returns a <see cref="Degree"/>.</returns>
    public Task<Degree> GetDegreeAsync(string degreeName);

    
    /// <summary>Asynchronously returns the course corresponding to given degree and course names.</summary>
    /// <param name="degreeName">Name of the degree that the course belongs to.</param>
    /// <param name="courseName">Name of the course.</param>
    /// <exception cref="T:System.ArgumentException">Given parameters form an invalid path.</exception>
    /// <exception cref="T:System.Collections.Generic.KeyNotFoundException">No corresponding course was found.</exception>
    /// <returns>A <see cref="Task{Course}"/> that upon completion returns a <see cref="Course"/>.</returns>
    public Task<Course> GetCourseAsync(string degreeName, string courseName);
    
    
    /// <summary>Asynchronously returns a tuple of a filestream and the MIME type of a file, given the degree, course and filename</summary>
    /// <param name="degreeName">Name of the degree that the course belongs to.</param>
    /// <param name="courseName">Name of the course that the file belongs to.</param>
    /// <param name="fileName">Name of the file.</param>
    /// <exception cref="T:System.ArgumentException">Given parameters form an invalid path.</exception>
    /// <exception cref="T:System.Collections.Generic.KeyNotFoundException">No corresponding file or directory was found.</exception>
    /// <returns>A <see cref="Task{Tuple}"/> that on completion returns a <see cref="Tuple"/> of types <see cref="FileStream"/> and <see cref="string"/></returns>
    public Task<(FileStream, string)> GetFileStreamAsync(string degreeName, string courseName, string fileName);
}