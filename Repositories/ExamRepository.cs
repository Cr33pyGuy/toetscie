using System.Globalization;
using System.Text.Encodings.Web;
using FMF.Toetscie.Models;
using FMF.Toetscie.Options;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Options;

namespace FMF.Toetscie.Repositories;

/*
    This repo wants filenames to be in the format [yyyy-MM-dd]_[type]_[author/lecturer name].pdf
    Examples: 
    2022-12-13_Exam + solutions_Diederik Roest.pdf
    2023-01-01_notes_Julian Gerritsen.pdf
    
    Magic types: "solutions", "notes" (case insensitive).
    If its type (see what "type" is in the naming convention above) is called "solutions", it will be listed under the 
    exam with the same date and course.
    If its type is called "notes", it will be listed under the notes of the course. Instead of the lecturer name,
    put the name of the author/student that made it.
    
    If it's anything else, it will be listed like an exam, and the front end will show the exact type that you gave it.
    So the type could be something like "Test 3" or "midterm" and it will show that as well.
    
    You can append another _ to the file name (before the extension) and everything after that will be ignored.
*/

public class ExamRepository : IExamRepository
{
    private readonly ExamRepositoryOptions _options;
    private readonly ILogger _logger;

    public ExamRepository(IOptions<ExamRepositoryOptions> options, ILogger<IExamRepository> logger)
    {
        _logger = logger;
        _options = options.Value;

        if (_options.ExamsFolder == string.Empty || !new DirectoryInfo(_options.ExamsFolder).Exists)
        {
            // This is automatically logged as the following exception is not caught anywhere
            throw new DirectoryNotFoundException($"ExamsFolder {_options.ExamsFolder} from app settings not found");
        }
    }

    // The other List functions are private but this one is used in the controller 
    // since the index page lists degrees and is not part of another model.
    public async Task<List<Degree>> ListDegreesAsync()
        => await Task.Run(() =>
            Directory
                .EnumerateDirectories(_options.ExamsFolder)
                .Select(path => new Degree
                {
                    Name = Path.GetFileName(path),
                    Route = Path.GetRelativePath(_options.ExamsFolder, path)
                })
                .ToList()
        );

    public async Task<Degree> GetDegreeAsync(string degreeName)
        => await Task.Run(() =>
            new Degree
            {
                Name = degreeName,
                Route = degreeName,
                Courses = ListCourses(degreeName)
            }
        );

    public async Task<Course> GetCourseAsync(string degreeName, string courseName)
        => await Task.Run(() =>
        {
            // This list of IFiles will have types: Exam, Note, Solution inside of it. Those are all implementations of IFile.
            var files = ListFiles(degreeName, courseName);
            var exams = files.OfType<Exam>().ToList(); // List because we add things below.

            // Find the exams that the solutions belong to
            foreach (var sol in files.OfType<Solution>())
            {
                var exam = exams.FirstOrDefault(e => e.Date == sol.Date);
                if (exam is not null)
                {
                    exam.Solutions = sol;
                }
                else
                {
                    exams.Add(new Exam
                    {
                        Date = sol.Date,
                        Solutions = sol
                    }); // Here we just give up and list it under a route-less exam.
                }
            }

            return new Course
            {
                Name = courseName,
                Route = Path.Combine(degreeName, courseName),
                Exams = exams.OrderByDescending(e => e.Date).ToList(),
                Notes = files.OfType<Note>().OrderByDescending(e => e.Date).ToList()
            };
        });

    public async Task<(FileStream, string)> GetFileStreamAsync(string degreeName, string courseName, string fileName)
        => await Task.Run(() =>
            HandlePath(
                Path.Combine(_options.ExamsFolder, degreeName, courseName, fileName), // fileDir
                fileDir =>
                {
                    new FileExtensionContentTypeProvider().TryGetContentType(fileDir, out var contentType);
                    return (new FileStream(fileDir, FileMode.Open),
                        contentType ?? "application/pdf");
                }
            )
        );

    private List<Course> ListCourses(string degreeName)
        => HandlePath(
            Path.Combine(_options.ExamsFolder, degreeName), // degreeDir
            degreeDir =>
                Directory
                    .EnumerateDirectories(degreeDir)
                    .Select(path => new Course
                    {
                        Name = Path.GetFileName(path),
                        Route = Path.GetRelativePath(_options.ExamsFolder, path)
                    })
                    .ToList()
        );


    private List<IFile?> ListFiles(string degreeName, string courseName)
        => HandlePath(
            Path.Combine(_options.ExamsFolder, degreeName, courseName), // courseDir
            courseDir =>
                Directory
                    .EnumerateFiles(courseDir)
                    .Select(GetFile)
                    .ToList()
        );

    private IFile? GetFile(string path)
    {
        var attributes = Path.GetFileNameWithoutExtension(path).Split("_");
        if (attributes.Length < 2) return null;

        IFile file = attributes[1].ToLower() switch
        {
            "solutions" => new Solution(),
            "notes" => new Note { Author = attributes.ElementAtOrDefault(2) },
            _ => new Exam { Author = attributes.ElementAtOrDefault(2) }
        };

        var dateParsed =
            DateTime.TryParseExact(attributes[0], "yyyy-MM-dd", CultureInfo.InvariantCulture, 0, out var date);
        file.Date = dateParsed ? date : null;
        file.Type = attributes[1];
        file.Route = Path.GetRelativePath(_options.ExamsFolder, path);

        return file;
    }

    private T HandlePath<T>(string dir, Func<string, T> callback)
    {
        // Check if we're still inside the exams folder
        if (Path.GetRelativePath(_options.ExamsFolder, dir).StartsWith(".."))
        {
            Exception newEx = new ArgumentException($"Invalid file path: {dir}");
            _logger.LogWarning(newEx, "Exception occurred");
            throw newEx;
        }

        try
        {
            return callback(dir); // Do whatever we want to do, which includes directory/file enumeration of the dir.
        }
        catch (IOException ex) when (ex is DirectoryNotFoundException or FileNotFoundException) // No logging needed
        {
            throw new KeyNotFoundException($"Object at at {dir} not found", ex);
        }
        catch (IOException ex)
        {
            Exception newEx = new ArgumentException($"Invalid file path: {dir}", ex);
            _logger.LogWarning(newEx, "Exception occurred");
            throw newEx;
        }
    }
}