using FMF.Toetscie.Repositories;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace FMF.Toetscie.Controllers;

[Controller]
public class HomeController : Controller
{
    private readonly IExamRepository _exams;

    public HomeController(IExamRepository exams)
    {
        _exams = exams;
    }

    public async Task<ActionResult> Index() 
        => View(await _exams.ListDegreesAsync());

    [HttpGet("/{degreePath}")]
    public async Task<ActionResult> DegreeView(string degreePath)
        => View(await _exams.GetDegreeAsync(degreePath));

    [HttpGet("/{degreePath}/{coursePath}")]
    public async Task<ActionResult> CourseView(string degreePath, string coursePath)
        => View(await _exams.GetCourseAsync(degreePath, coursePath));

    [HttpGet("/{degreePath}/{coursePath}/{examPath}")]
    public async Task<ActionResult> ExamView(string degreePath, string coursePath, string examPath)
    {
        var (fs, mime) = await _exams.GetFileStreamAsync(degreePath, coursePath, examPath);
        return File(fs, mime);
    }

    [HttpGet("/Error")] // Our error handler. This is not used in the dev environment.
    public ActionResult Error()
    {
        // Make sure to add exceptions that IExamRepository throws here.
        switch (HttpContext.Features.Get<IExceptionHandlerFeature>()?.Error)
        {
            case ArgumentException:
                HttpContext.Response.StatusCode = 400;
                ViewData["Message"] = "Invalid URI";
                break;
            case KeyNotFoundException:
                HttpContext.Response.StatusCode = 404;
                ViewData["Message"] = "Requested resource not found";
                break;
            default:
                HttpContext.Response.StatusCode = 500;
                ViewData["Message"] = "Internal server error";
                break;
        }
        return View();
    }
}